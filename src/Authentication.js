import React, {Component} from 'react';
import Usurvey from './Usurvey'
import './Authentication.css';
var firebase = require('firebase')

class Authentication extends Component {
  constructor(props){
    super(props);
    this.emailRef = React.createRef();
    this.passRef = React.createRef();
    this.state = {
      error: '',
      isLogged: false,
      email: '',
      uid: ''
    };
  }

  login(event){
    const self = this;
    const email = this.emailRef.current.value;
    const password = this.passRef.current.value;
    const promise = firebase.auth().signInWithEmailAndPassword(email, password)
    .then(function(result) {
      self.setState(({email: result.user.email, isLogged: true, uid: result.user.uid}))
    }).catch(function(error) {
      console.log(error)
      self.setState({error: error.message})
    });
  }

  signup(){
    const self = this;
    const email = this.emailRef.current.value;
    const password = this.passRef.current.value;
    const promise = firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(function(result) {
      self.setState({error: "Please signin to continue"});
    }).catch(function(error) {
      self.setState({error: error.message})
    });
  }

  google(){
    const self = this;
    var provider = new firebase.auth.GoogleAuthProvider();
    var promise = firebase.auth().signInWithPopup(provider);

    promise.then(result => {
      var user = result.user;
      console.log(result);
      firebase.database().ref('users/'+user.uid).set({
        email: user.email,
        name: user.displayName
      });
      self.setState(({email: user.email, isLogged: true, uid: user.uid}))
    });
    promise.catch(error => {
      console.log(error)
      self.setState({error: error.message})
    })
  }

  render(){
    return(
      <div>
        { this.state.email === '' ? <div>
            <input type="email" placeholder="Enter your email" id="email" ref={this.emailRef} /><br />
            <input type="password" placeholder="Enter your password" id="pass" ref={this.passRef} /><br />
            <p>{this.state.error}</p>
            <button onClick={this.login.bind(this)}>Log In</button>
            <button onClick={this.signup.bind(this)}>Sign Up</button><br />
            <button onClick={this.google.bind(this)}>Sign In With Google</button>
          </div>
          :
          <Usurvey uid={this.state.uid} />
        }

      </div>
    );
  }
}

export default Authentication;
