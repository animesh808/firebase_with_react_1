import React, { Component } from 'react';
var firebase = require('firebase')


class Usurvey extends Component {
  constructor(props){
    super(props);
    this.myRef = React.createRef();
    this.state = {
      uid: this.props.uid,
      studentName: '',
      answers: {
        answer1: '',
        answer2: '',
        answer3: ''
      },
      isSubmitted: false
    };
    this.answerSelected = this.answerSelected.bind(this)
  }

  submitForm(event){
    this.setState({studentName: this.myRef.current.value});
  }

  answerSelected(event){
    var answers = this.state.answers;
    if(event.target.name === Object.keys(answers)[0]){
      answers.answer1 = event.target.value;
    }else if (event.target.name === Object.keys(answers)[1]) {
      answers.answer2 = event.target.value;
    }else if (event.target.name === Object.keys(answers)[2]) {
      answers.answer3 = event.target.value;
    }
    this.setState({answers: answers})
  }

  answerSubmit(){
    var self = this
    var userId = self.state.uid
    console.log(userId)
    firebase.database().ref('uSurvey/' + userId).set({
      studentName: self.state.studentName,
      answers: self.state.answers
    });
    
    this.setState({isSubmitted: true})
  }

  logout(){
    firebase.auth().signOut();
    this.setState({email: '', isLogged: false})
  }

  render(){
    var studentName;
    var questions;

    if (this.state.studentName === '' && this.state.isSubmitted === false) {
      studentName = <div>
        <h1>Hello Student, please let us your name: </h1>
        <form onSubmit={this.submitForm.bind(this)}>
          <input className='namy' type="text" placeholder="Enter your name here" ref={this.myRef} /> <br />
          <button onClick={this.logout.bind(this)}>Log Out</button>
        </form>
      </div>
    } else if (this.state.studentName !== '' && this.state.isSubmitted === false) {
      questions = <div><h2>Wellcome to Usurvey, {this.state.studentName}</h2>
      <form>
        <div className="card">
          <label>What type of courses you like?</label><br/>
          <input type="radio" name="answer1" value="Technology" onChange={this.answerSelected} />Technology
          <input type="radio" name="answer1" value="Design" onChange={this.answerSelected} />Design
          <input type="radio" name="answer1" value="Marketing" onChange={this.answerSelected} />Marketing
        </div>

        <div className="card">
          <label>What is your current profession?</label><br/>
          <input type="radio" name="answer2" value="Student" onChange={this.answerSelected} />Student
          <input type="radio" name="answer2" value="Graduated" onChange={this.answerSelected} />Graduated
          <input type="radio" name="answer2" value="Professional" onChange={this.answerSelected} />Professional
        </div>

        <div className="card">
          <label>Is online learning helpful?</label><br/>
          <input type="radio" name="answer3" value="Yes" onChange={this.answerSelected} />Yes
          <input type="radio" name="answer3" value="No" onChange={this.answerSelected} />No
          <input type="radio" name="answer3" value="Maybe" onChange={this.answerSelected} />Maybe
        </div>

        <input className="feedback-button" type="submit" onClick={this.answerSubmit.bind(this)} />
      </form></div>
  } else if (this.state.studentName !== '' && this.state.isSubmitted === true) {
    studentName = <div>
      <h2>Thanks {this.state.studentName}.</h2>
      <button onClick={this.logout.bind(this)}>Log Out</button>
    </div>
  }
    return(
      <div>
        {studentName}
        --------------------------------------------------
        {questions}
      </div>
    );
  }
}

export default Usurvey;
